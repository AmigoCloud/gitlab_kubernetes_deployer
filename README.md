# Gitlab Kubernetes Deployer

Docker image with scripts to assist with deploying from Gitlab to Kubernetes.  

# Prerequesits

### Environments 

Environment variables are stored in three places: The Organization's Settings, Project's Settings, and the Gitlab CICD Runner  

Organization level settings are only visible to a few people and sensitive secrets are hidden for security reasons.   

Project level settings are used to assist with deploying to individual environments.   

Gitlab CICD Environment Variables assist with communication between stages, reporting, and the registry.   


These scripts rely on specific Environment Variables to be set, and can be overridden in your gitlab cicd by using this syntax: 

`ENVIRONMENT_VARIABLE=VALUE script-to-run`

To the extent possible there should be no need to this, but it is available to allow for flexibility. 


# Scripts


## login-to-gitlab

Shell Script Syntatic Sugar for logging into gitlab's registry. Equivalent to `docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY`

**Required Variables**:  

| VARIABLE | LOCATION | 
| -------- | -------- |
| CI_JOB_TOKEN | Gitlab CICD Runner |



## generate-values-file


This is a python script (rather than a shell script). When declaring variables in your project, you can have two variables with the same
name that are set for different environments. This allows you to specify a namespace, or FQDN that is different between your staging server
and your production server. Furthermore, you may already have the value specified in your helm chart's `values.yaml` file where the default
value there is used for the development environment. 

```
usage: generate-values-file [-h] [--input-filename INPUT_FILE]
                            [--output-filename OUTPUT_FILE] [--print-result]

This script will take the Environment Variables in the current environment,
then open the INPUT_FILE and replace any matching values in that INPUT_FILE
with the ones from the Environment Variables. Finally, it will write the
result to the OUTPUT_FILE. If the OUTPUT_FILE is not set, it will override
the INPUT_FILE with the result.

optional arguments:
  -h, --help            show this help message and exit
  --input-filename INPUT_FILE
  --output-filename OUTPUT_FILE
  --print-result        Prints the output of the final file for debugging

```


##  setup-namespace <namespace>

This shell script does two things: 

First, it will create the specified Kubernetes namespace if it is not already present
Second, it will add a docker registry secret into the namespace allowing any Kubernetes Controllers to pull images from the gitlab registry


**Required Variables**:  

| VARIABLE | LOCATION |
| -------- | -------- |
| CI_REGSITRY | Gitlab CICD Runner |
| AMIGOBUILD_USERNAME | Organization Settings |
| AMIGOBUILD_GITLAB_REGISTRY_ACCESS_TOKEN | Organization Settings |
| AMIGOBUILD_EMAIL | Organization Settings |



# Updating the image

To update the image do the following: 
- Install skaffold
- Login to gitlab's registry `docker login registry.gitlab.com`
- Build a local image with `skaffold build`  
- Test the image locally
- Run the command `VERSION=<new version> skaffold build -p release`  for example `VERSION=1.6.0 skaffold build -p release`
- The image will be built locally, then uploaded to the gitlab repo. 
- in Gitlab, create a tag with the new version name and release notes describing the improvement for that version. 


