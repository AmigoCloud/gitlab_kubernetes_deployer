FROM docker:20.10.9

WORKDIR /app
ENV DOCKER_HOST=tcp://docker:2375/
RUN apk update && apk add python3 curl bash openssh-client vim bind-tools

ARG KUBECTL_VERSION=1.16.3 
ARG HELM_VERSION=3.9.0
ARG SKAFFOLD_VERSION=1.38.0

# install kubectl (https://github.com/kubernetes/kubectl)
RUN apk add -U openssl curl tar gzip bash ca-certificates git
RUN curl -Lo /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl
RUN chmod +x /usr/local/bin/kubectl

# install helm (https://github.com/helm/helm)
RUN curl -Lo helm.tar.gz https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz
RUN tar zxvf helm.tar.gz && rm helm.tar.gz
RUN chmod +x linux-amd64/helm
RUN mv linux-amd64/helm /usr/local/bin

# install skaffold (https://github.com/GoogleContainerTools/skaffold)
RUN curl -Lo /usr/local/bin/skaffold https://storage.googleapis.com/skaffold/releases/v${SKAFFOLD_VERSION}/skaffold-linux-amd64
RUN chmod +x /usr/local/bin/skaffold

# setup scripts
COPY . .
RUN mv generate-values-file setup-namespace login-to-gitlab /usr/local/bin

# install pylint and pylint-junit to support linting
# install fastly to support purging the fastly cdn as part of deployment
RUN apk update && apk add gcc vim python3-dev linux-headers musl-dev
RUN wget https://bootstrap.pypa.io/get-pip.py && python3 get-pip.py
RUN python3 -m pip install pylint pylint-junit fastly 

# install GSUtil to allow for grabbing install files from google cloud storage buckets when 
# needed for building images. 
RUN curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz > /tmp/google-cloud-sdk.tar.gz \
  && mkdir -p /usr/local/gcloud \
  && tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz \
  && /usr/local/gcloud/google-cloud-sdk/install.sh --quiet \
  && rm  /tmp/google-cloud-sdk.tar.gz 

# Add gsutil path to local path for calling the command in build scripts
ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin
